package com.poc.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.poc.game.screens.FPSTest;
import com.poc.game.screens.Menu;
import com.poc.game.screens.Sandbox;
import com.poc.game.screens.cylinderWorld.CylinderWorldTest;
import com.poc.game.screens.screenSize.ScreenSizeTest;
import com.poc.game.screens.autotile.AutotileDepthTest;
import com.poc.game.screens.autotile.AutotileTest;
import com.poc.game.screens.spriteStacking.SpriteStackingMapTest;
import com.poc.game.screens.spriteStacking.SpriteStackingPerspectiveTest;
import com.poc.game.screens.spriteStacking.SpriteStackingPlatformerTest;
import com.poc.game.screens.spriteStacking.SpriteStackingTest;
import com.poc.game.screens.unifont.*;

public class POC extends ApplicationAdapter
{
    public OrthographicCamera cam;
    public Viewport view;
    private Screen[] screens;
    private int curScreen;

    public Screen[] GetScreens()
    {
        final Screen[] s = screens;
        return s;
    }
    public void SetScreen(int i)
    {
        curScreen = i;
    }

    @Override
    public void create()
    {
        cam = new OrthographicCamera();
        view = new FitViewport(240, 160, cam);
        view.apply();
        screens = new Screen[]
                {
                        new Menu(this),
                        new UnifontTest(this),
                        new AutotileTest(this),
                        new AutotileDepthTest(this),
                        new ScreenSizeTest(this),
                        new SpriteStackingTest(this),
                        new SpriteStackingMapTest(this),
                        new SpriteStackingPerspectiveTest(this),
                        new SpriteStackingPlatformerTest(this),
                        new FPSTest(this),
                        new CustomfontTest(this),
                        new ExtendedUnifontTest(this),
                        new CylinderWorldTest(this),
                        new Unifont2Test(this),
                        new Unifont3Test(this),
                        new Sandbox(this)
                }; //Menu();

        curScreen = 0;
    }

    @Override
    public void dispose()
    {
        for (Screen s : screens)
        {
            s.dispose();
        }
    }

    @Override
    public void render()
    {
        cam.update();
        screens[curScreen].render(Gdx.graphics.getDeltaTime());
    }

    @Override
    public void resize(int width, int height)
    {
        view.update(width, height);
        screens[curScreen].resize(width,height);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
    }
}
