package com.poc.game.screens.cylinderWorld;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class CylinderTile extends Sprite implements Comparable<CylinderTile>
{
    private Float depth;

    public float GetDepth()
    {
        return depth;
    }

    public void SetDepth(float depth)
    {
        this.depth = depth;
    }

    public CylinderTile (Texture texture, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(texture, srcX, srcY, srcWidth, srcHeight);
        super.setPosition(x,y);
    }

    @Override
    public int compareTo(CylinderTile o)
    {
        return depth.compareTo(o.depth);
    }
}
