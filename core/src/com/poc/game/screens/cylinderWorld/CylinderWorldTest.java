package com.poc.game.screens.cylinderWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CylinderWorldTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private Texture cylTiles;
    private List<CylinderTile> grassTiles;
    private List<CylinderTile> roadTiles;
    private List<Float> depthTiles;
    private float depthOffset;
    byte[][] map;

    private POC root;

    @Override
    public String toString()
    {
        return "[WIP] Cylinder World";
    }

    public CylinderWorldTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        cylTiles = new Texture("CylTiles.png");
        map = new byte[][]
                {
                        {0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {1, 1, 0, 1, 1, 1, 0, 1, 1},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1, 0},
                        {0, 1, 1, 1, 0, 1, 1, 1, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0}
                };
        grassTiles = new ArrayList<CylinderTile>();
        roadTiles = new ArrayList<CylinderTile>();
        depthTiles = new ArrayList<Float>();
        depthOffset = 0;
        for (int i = 0; i < 135; i++)
        {
            depthTiles.add(0f);
        }
        for (int x = 0; x < map.length; x++)
        {
            for (int y = 0; y < map[0].length; y++)
            {
                switch (map[x][y])
                {
                    case 0:
                        grassTiles.add(new CylinderTile(cylTiles, x, y, 0, 0, 16, 16));
                        break;
                    case 1:
                        roadTiles.add(new CylinderTile(cylTiles, x, y, 0, 16, 16, 16));
                        break;
                }
            }
        }

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }
        depthOffset += delta;

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        for (int x = 0; x < map.length; x++)
        {
            for (int y = 0; y < map[0].length; y++)
            {
                try
                {
                    grassTiles.get(y * map[x].length + x).SetDepth(depthOffset);
                }
                catch (Exception e)
                {

                }
                try
                {
                    roadTiles.get(y * map[x].length + x).SetDepth(depthOffset);
                }
                catch (Exception e)
                {

                }
            }
        }
        Collections.sort(grassTiles);
        Collections.sort(roadTiles);
        for (CylinderTile g : grassTiles)
        {
            batch.draw(g, g.getX() * 16, 32 * MathUtils.sin(g.GetDepth() + g.getY() * 16));
        }
        for (CylinderTile r : roadTiles)
        {
            //batch.draw(r, r.getX() * 16, 16 * MathUtils.sin(r.GetDepth() + r.getY()));
        }
        //ufl.DrawString(batch, new Vector2(8, 160 - 8)/*view.getWorldHeight() - 1 - 8)*/, "Roman, ελληνικά, ϯⲙⲉⲧⲣⲉⲙⲛ̀ⲭⲏⲙⲓ / ⲧⲙⲛ̄ⲧⲣⲙ̄ⲛ̄ⲕⲏⲙⲉ, Кириллица", Color.WHITE, 0.5f);
        //ufl.DrawString(batch, new Vector2(8, 160 - 16)/*view.getWorldHeight() - 1 - 8)*/, "հայերէն / հայերեն, देवनागरी", Color.WHITE, 0.5f, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
