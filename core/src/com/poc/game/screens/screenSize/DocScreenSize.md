# Screen Size
## Description
There's a certain appeal about old games that retro / retraux games strive to emulate while sticking to modern conventions. One of these conventions which I'm not a fan of is a lack of the tiny screens of handheld games. This test is to compare various handheld resolutions for feasible usage in future retro / retraux games.
## Process
1) set view size
2) resize window to fit new view size