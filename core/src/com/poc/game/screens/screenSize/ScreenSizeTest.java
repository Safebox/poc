package com.poc.game.screens.screenSize;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

public class ScreenSizeTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;
    private Pixmap screen;

    private POC root;

    @Override
    public String toString()
    {
        return "Screen Size";
    }

    public ScreenSizeTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();
        //screen = new Pixmap(240,160, Pixmap.Format.RGBA8888);

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.view.setWorldSize(240, 160);
            root.resize(240, 160);
            Gdx.graphics.setWindowedMode(240, 160);
            root.SetScreen(0);
            return;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1))
        {
            root.view.setWorldSize(160, 144);
            root.resize(160, 144);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2))
        {
            root.view.setWorldSize(240, 160);
            root.resize(240, 160);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3))
        {
            root.view.setWorldSize(256, 192);
            root.resize(256, 192);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_4))
        {
            root.view.setWorldSize(256, 480);
            root.resize(256, 480);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_5))
        {
            root.view.setWorldSize(480,272);
            root.resize(480,272);
        }
        screen.setColor(Color.WHITE);
        screen.fill();
        if (root.view.getScreenWidth() == 256 && root.view.getScreenHeight() == 480)
        {
            screen.setColor(Color.BLACK);
            screen.fillRectangle(0,192,256,96);
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        batch.draw(new Texture(screen), 0,0);
        ufl.DrawString(batch, new Vector2(0, root.cam.viewportHeight)/*view.getWorldHeight() - 1 - 8)*/, "[1] Game Boy\n[2] Game Boy Advance\n[3] Nintendo DS (one screen)\n[4] Nintendo DS (both screens)\n[5] PlayStation Portable", Color.BLACK);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        screen = new Pixmap((int)root.view.getWorldWidth(), (int)root.view.getWorldHeight(), Pixmap.Format.RGBA8888);
        Gdx.graphics.setWindowedMode(width, height);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
