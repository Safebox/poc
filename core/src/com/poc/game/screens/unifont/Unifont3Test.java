package com.poc.game.screens.unifont;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoaderV3;

public class Unifont3Test implements Screen
{
    private SpriteBatch batch;
    private UnifontLoaderV3 ufl;

    private POC root;

    @Override
    public String toString()
    {
        return "Unifont V3";
    }

    public Unifont3Test(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoaderV3();

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        ufl.drawString(batch, (ScalingViewport)root.view, new Rectangle(8, 160 - 8, 240 - 32, 32), 0, "{*Unifont Loader V3*}", Color.WHITE);
        ufl.drawString(batch, (ScalingViewport)root.view, new Rectangle(8, 160 - 32, 240 - 32, 8 * 9), 0, "Normal        {*Bold*} {/Italic/} {_Underline_} {-Strikethrough-}\n\n" +
                "Bold               {*{/Italic/}*} {*{_Underline_}*} {*{-Strikethrough-}*}\n\n" +
                "Italic        {/{*Bold*}/}        {/{_Underline_}/} {/{-Strikethrough-}/}\n\n" +
                "Underline     {_{*Bold*}_} {_{/Italic/}_}           {_{-Strikethrough-}_}\n\n" +
                "Strikethrough {-{*Bold*}-} {-{/Italic/}-} {-{_Underline_}-}", Color.WHITE, 0.5f);
        ufl.drawString(batch, (ScalingViewport)root.view, new Rectangle(8, 64 - 16, (240 * 0.6f - 8) + ((240 * 0.4f - 8) * (0.5f + 0.5f * (float)Math.sin(System.currentTimeMillis() * 0.001))), 8 * 5), 0, "{*{0}*}, also known as {1}, is breaking a section of text into lines so that it will fit into the available width of a page, window or other display area.", Color.WHITE, 0.5f, "Line breaking", "{*word wrapping*}");
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
