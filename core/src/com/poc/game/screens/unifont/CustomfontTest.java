package com.poc.game.screens.unifont;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.CustomfontLoader;
import utilities.InputGBA;

public class CustomfontTest implements Screen
{
    private SpriteBatch batch;
    private CustomfontLoader cfl;

    private POC root;

    @Override
    public String toString()
    {
        return "Custom Unifont";
    }

    public CustomfontTest(POC root)
    {
        batch = new SpriteBatch();
        cfl = new CustomfontLoader("KanaBasic.png","CharList.txt");

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        cfl.DrawString(batch, new Vector2(8, 160 - 8), "カスタム・フォント・ローダ", Color.WHITE, true);
        cfl.DrawString(batch, new Vector2(8, 160 - 48), "カスタム・フォント・ローダ　ハ　ワタシ　ノ　タ　ノ\n\n" +
                "プロジェクト、　ユニ・フォント・ローダ　ニ　モトヅイテ\n\n" +
                "イマス。\n\n\n\n" +
                "「カスタム・フォント・ローダ」・ハ・ワタシ・ノ・タ・ノ\n\n" +
                "プロジェクト、「ユニ・フォント・ローダ」・ニ・モトヅイテ\n\n" +
                "イマス。", Color.WHITE, 0.5f, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        //Gdx.gl.glViewport(Gdx.graphics.getWidth() / 2 - (int)root.view.getWorldWidth() / 2, Gdx.graphics.getHeight() / 2 - (int)root.view.getWorldHeight() / 2, (int)root.view.getWorldWidth(), (int)root.view.getWorldHeight());
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
