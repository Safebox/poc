package com.poc.game.screens.unifont;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoaderV2;

public class Unifont2Test implements Screen
{
    private SpriteBatch batch;
    private UnifontLoaderV2 ufl;

    private POC root;

    @Override
    public String toString()
    {
        return "Unifont V2";
    }

    public Unifont2Test(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoaderV2();

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        ufl.drawString(batch, new Vector2(8, 160 - 8), "*Unifont Loader V2*", Color.WHITE);
        ufl.drawString(batch, new Vector2(8, 160 - 24), "_GNU Unifont_ is part of the /GNU Project/.\n\n" +
                "Το GNU Unifont αποτελεί μέρος του έργου GNU.\n\n" +
                "GNU Unifont\tявляется частью проекта GNU.\n\n" +
                "\tGNU Unifont- ը GNU ծրագրի մի մասն է:", Color.WHITE, 0.5f);
        ufl.drawString(batch, new Vector2(8, 160 - 24 - (8 * 8)), "GNU Unifont는 GNU 프로젝트의 일부입니다.\n\n" +
                "\tGNU Unifont是GNU项目的一部分。\n\n" +
                "GNU Unifont是GNU項目的一部分。\n\n" +
                "GNU UnifontはGNUプロジェクトの一部です。", Color.WHITE, 0.5f, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
