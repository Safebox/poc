package com.poc.game.screens.unifont;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

public class UnifontTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private POC root;

    @Override
    public String toString()
    {
        return "Unifont";
    }

    public UnifontTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        ufl.DrawString(batch, new Vector2(8, 160 - 8), "Unifont Loader", Color.WHITE);
        ufl.DrawString(batch, new Vector2(8, 160 - 24), "GNU Unifont is part of the GNU Project.\n\n" +
                "Το GNU Unifont αποτελεί μέρος του έργου GNU.\n\n" +
                "GNU Unifont является частью проекта GNU.\n\n" +
                "GNU Unifont- ը GNU ծրագրի մի մասն է:", Color.WHITE, 0.5f);
        ufl.DrawString(batch, new Vector2(8, 160 - 24 - (8 * 8)), "GNU Unifont는 GNU 프로젝트의 일부입니다.\n\n" +
                "GNU Unifont是GNU项目的一部分。\n\n" +
                "GNU Unifont是GNU項目的一部分。\n\n" +
                "GNU UnifontはGNUプロジェクトの一部です。", Color.WHITE, 0.5f, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
