# Unifont Loader
## Description
Almost every content type can be loaded into MonoGame without using their Content Pipeline, the sole exception to this is fonts which are treated like Code Pages with a limited number of predefined characters. In its originaly C# iteration, my Unifont Loader aimed to counter that by allowing for the loading of bitmap fonts, with the default GNU Unifont allowing for all 65536 characters of Unicode’s Basic Multilingual Plane 0. In the first version, right-to-left and top-to-bottom languages as well as ones with diacritics didn't work as characters were placed as separate tiles rather than overlapping one another. Some with right side diacritics did work to a degree but appeared abnormal.

Since then, I modified the loader to make a record of all diacritics, characters that are not a character on their own but make up a combining character when used with another “proper” character. Examples of this include the tilde above the n in Spanish and the cedilla under the c in French. When characters are parsed for display, any character that matches one in the list is drawn but the cursor’s position is not incremented so that said character is drawn at the same position as the last one.
## Process
### V1
**Identify Character Coordinates**
1) put each character coordinates into a <char, rectangle> dictionary
2) for diacritics, add them into an additional <char, rectangle> dictionary

**Draw Characters in String**
3) retrieve the coordinates for each character in string
4) draw said characters to the screen based on their coordinates
5) for diacritics, don't increase the cursor position
6) for the tab character, increase the cursor position by three
7) for new line characters, reset the cursor position and increase the line count

### V2
**Identify Character Coordinates**
1) put each character coordinates into a <char, rectangle> dictionary
2) for diacritics, add them into an additional <char, rectangle> dictionary

**Separate Formatting into Tokens**
3) draw characters twice for `*bold*`
4) draw characters with shear transform for `/italic/`
5) draw 0x0332 for `_underline_`

**Draw Characters in Token**
6) retrieve the coordinates for each character in token
7) draw said characters to the screen based on their coordinates
8) for diacritics, don't increase the cursor position
9) for the tab character, increase the cursor position by two (four spaces at half-width, two spaces at full-width)
10) for new line characters, reset the cursor position and increase the line count

### V3
**Identify Character Coordinates**
1) put each character coordinates into a <char, rectangle> dictionary
2) for diacritics, add them into an additional <char, rectangle> dictionary

**Substitute Arguments**
3) replace `{#}` with the corresponding argument if it exists

**Separate Formatting into Tokens**
4) draw characters twice for `{*bold*}`
5) draw characters with shear transform for `{/italic/}`
6) draw 0x0332 for `{_underline_}`
7) draw 0x0336 for `{-strikethrough-}`

**Draw Characters**
7) retrieve the coordinates for each character
8) draw said characters to the screen based on their coordinates
9) for diacritics, don't increase the cursor position
10) for the tab character, increase the cursor position by two (four spaces at half-width, two spaces at full-width)
11) for new line characters, reset the cursor position and increase the line count