package com.poc.game.screens.unifont;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.ExtendedUnifontLoader;
import utilities.InputGBA;

public class ExtendedUnifontTest implements Screen
{
    private SpriteBatch batch;
    private ExtendedUnifontLoader eufl;

    private POC root;

    public ExtendedUnifontTest(POC root)
    {
        batch = new SpriteBatch();
        eufl = new ExtendedUnifontLoader();
        eufl.AddTypewriter(0);
        eufl.AddTypewriter(1);
        eufl.AddTypewriter(2);
        eufl.AddTypewriter(3);

        this.root = root;
    }

    @Override
    public String toString()
    {
        return "Extended Unifont";
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        eufl.IncrementTypewriter(0, 0.1f);
        eufl.IncrementTypewriter(1, 1);
        if (eufl.GetTypewriter(1) > 402)
        {
            eufl.IncrementTypewriter(2, 0.1f);
        }
        if (eufl.GetTypewriter(2) > 24)
        {
            eufl.IncrementTypewriter(3, 1);
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        eufl.DrawString(batch, new Vector2(8, 160 - 8), "The Last Question\n" +
                "by Isaac Asimov", 0, Color.WHITE);
        eufl.DrawString(batch, new Vector2(8, 160 - 48), "But there was now no man to whom AC might give the answer\n" +
                "of the last question. No matter. The answer -- by\n" +
                "demonstration -- would take care of that, too.\n" +
                "\n" +
                "For another timeless interval, AC thought how best to do\n" +
                "this. Carefully, AC organized the program.\n" +
                "\n" +
                "The consciousness of AC encompassed all of what had once\n" +
                "been a Universe and brooded over what was now Chaos. Step\n" +
                "by step, it must be done.\n" +
                "\n" +
                "And AC said, ", 1, Color.WHITE, 0.5f);
        eufl.DrawString(batch, new Vector2(8 + (4 * 13), 24), "\"LET THERE BE LIGHT!\"", 2, Color.WHITE, 0.5f);
        eufl.DrawString(batch, new Vector2(8, 8), "And there was light----", 3, Color.WHITE, 0.5f);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
