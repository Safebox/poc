package com.poc.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

public class Menu implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;
    private int curSelection;

    private POC root;

    public Menu(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();
        curSelection = 1;

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonUP))
        {
            curSelection = MathUtils.clamp(curSelection - 1, 1, root.GetScreens().length - 2);
        }
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonDOWN))
        {
            curSelection = MathUtils.clamp(curSelection + 1, 1,root.GetScreens().length - 2);
        }
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonA))
        {
            root.SetScreen(curSelection);
            root.resize(root.view.getScreenWidth(), root.view.getScreenHeight());
            return;
        }
        if (Gdx.input.isKeyPressed(InputGBA.ButtonL) && Gdx.input.isKeyPressed(InputGBA.ButtonR))
        {
            root.SetScreen(root.GetScreens().length - 1);
            root.resize(root.view.getScreenWidth(), root.view.getScreenHeight());
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        for (int i = 1; i < root.GetScreens().length - 1; i++)
        {
            ufl.DrawString(batch, new Vector2(8, 160 - (8 * i)), i + " - " + root.GetScreens()[i].toString(), Color.WHITE, 0.5f);
        }
        ufl.DrawString(batch, new Vector2(240-24-(4 * (String.valueOf(root.GetScreens().length - 1).length() - 1)), 16), curSelection + " / " + (root.GetScreens().length - 2), Color.WHITE,0.5f);
        ufl.DrawString(batch, new Vector2(240-16, 160 - (8 * curSelection)), "＜", Color.WHITE,0.5f);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
