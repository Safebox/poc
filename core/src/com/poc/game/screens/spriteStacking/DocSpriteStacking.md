# Sprite Stacking
## Description
I previously made an attempt to replicate the style of voxel games in 2D but was unsure how to approach the problem. I recently came across an effect known as sprite stacking which is starting to be used in Game Maker Studio games to replicate a 3D effect without using 3rd party libraries.

This is my attempt to replicate the effect in libGDX.
## Process
**Convert 3D Model to a Texture Atlas**
1) open model in MagicaVoxel (or other voxel program)
2) use command "o slice" (or other voxel program's slice function)

**Load Slices**
3) load texture atlas into TextureAtlas variable
4) for each slice, add region
5) for each slice, set origin to center

**Draw Slices**
6) for each slice, draw with grey tint at (given X, given Y + slice ID)
6) for each slice, draw with white tint at (given X, given Y + slice ID + 1)