package com.poc.game.screens.spriteStacking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.poc.game.POC;
import utilities.InputGBA;

public class SpriteStackingTest implements Screen
{
    private SpriteBatch batch;
    private TextureAtlas gate;
    private float rot;

    private POC root;

    @Override
    public String toString()
    {
        return "Sprite Stacking";
    }

    public SpriteStackingTest(POC root)
    {
        batch = new SpriteBatch();
        gate = new TextureAtlas();
        Texture texture = new Texture("Gate.png");
        for (int i = 0; i < 96; i++)
        {
            gate.addRegion(String.valueOf(i),  texture, 0, 388 - 4 - 4 * i, 96, 4);
        }
        rot = 0;

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }
        rot += delta;

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        for (int i = 0; i < gate.getRegions().size; i++)
        {
            String s = String.valueOf(i);
            Sprite gateSlice = gate.createSprite(s);
            gateSlice.setOrigin(gateSlice.getWidth() / 2, gateSlice.getHeight() / 2);
            gateSlice.setPosition(root.view.getWorldWidth() / 2 - gateSlice.getWidth() / 2, root.view.getWorldHeight() / 2 - gateSlice.getWidth() / 2 + i);
            gateSlice.setRotation(-rot / 3.14f * 180f);
            gateSlice.setColor(Color.LIGHT_GRAY);
            gateSlice.draw(batch);
            gateSlice.setPosition(root.view.getWorldWidth() / 2 - gateSlice.getWidth() / 2, root.view.getWorldHeight() / 2 - gateSlice.getWidth() / 2 + i + 1);
            gateSlice.setColor(Color.WHITE);
            gateSlice.draw(batch);
            //batch.draw(gateSlice.getTexture(), 0, i, gateSlice.getOriginX(), gateSlice.getOriginY(), gateSlice.getRegionWidth(), gateSlice.getRegionHeight(), 1, 1, rot, 0, 0, gateSlice.getWidth(), gateSlice.getHeight());
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        //Gdx.gl.glViewport(Gdx.graphics.getWidth() / 2 - (int)root.view.getWorldWidth() / 2, Gdx.graphics.getHeight() / 2 - (int)root.view.getWorldHeight() / 2, (int)root.view.getWorldWidth(), (int)root.view.getWorldHeight());
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
