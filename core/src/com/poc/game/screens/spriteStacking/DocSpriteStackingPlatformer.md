# Sprite Stacking Platformer
## Description
In most 2D platformers, 3D depth is achieved by applying a parallax effect to the background at relative intensities depending on the distance wanting to be portrayed. Sprite stacking could improve upon this effect by applying it to the platform itself. To the best of my knowledge, the effect has never been applied in such a manner before.
## Process
**Convert 3D Model to a Texture Atlas**
1) open model in MagicaVoxel (or other voxel program)
2) use command "o slice" (or other voxel program's slice function)

**Load Slices**
3) load texture atlas into TextureAtlas variable
4) for each slice, add region

**Draw Slices**
5) lerp offset towards the player direction, 1 is right and -1 is left
6) for each slice, draw at (given X - (offset * slice ID), given Y)