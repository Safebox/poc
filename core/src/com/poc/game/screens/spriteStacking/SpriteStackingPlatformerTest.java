package com.poc.game.screens.spriteStacking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

import java.text.DecimalFormat;

public class SpriteStackingPlatformerTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private TextureAtlas platformTexture;
    private Sprite[] platformLayers;
    byte[][] map;
    Vector2 playerPos;
    boolean lookingLeft;
    float offset;

    private POC root;

    @Override
    public String toString()
    {
        return "Sprite Stacking Platformer";
    }

    public SpriteStackingPlatformerTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        Texture texture = new Texture("PlatformTest.png");
        platformTexture = new TextureAtlas();
        platformLayers = new Sprite[16];
        for (int i = 0; i < 16; i++)
        {
            platformTexture.addRegion(String.valueOf(i), texture, 0, (16 * 16) - 16 - (16 * i), 16, 16);
            platformLayers[i] = platformTexture.createSprite(String.valueOf(i));
        }
        map = new byte[][]
                {
                        {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                        {1, 1, 0, 0, 0, 0, 0, 1, 0, 0},
                        {1, 1, 0, 0, 0, 0, 1, 1, 0, 0},
                        {1, 1, 0, 0, 0, 0, 1, 1, 0, 0},
                        {1, 1, 1, 0, 0, 0, 1, 1, 0, 0},
                        {1, 1, 1, 0, 0, 0, 0, 1, 0, 0},
                        {1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
                        {1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
                        {1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
                        {1, 1, 1, 0, 0, 0, 0, 1, 0, 0},
                        {1, 1, 1, 0, 0, 0, 1, 1, 0, 0},
                        {1, 1, 0, 0, 0, 0, 1, 1, 0, 0},
                        {1, 1, 0, 0, 0, 0, 1, 1, 0, 0},
                        {1, 1, 0, 0, 0, 0, 0, 1, 0, 0},
                        {1, 1, 0, 0, 0, 0, 0, 0, 0, 0}
                };
        playerPos = new Vector2(Math.round((240 / 2f) / 16f) - 1, Math.round((160 / 2f) / 16f) - 1);
        lookingLeft = false;
        offset = 0;

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }
        Vector2 newPos = new Vector2(playerPos.x, playerPos.y);
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonUP))
        {
            newPos.y++;
        }
        else if (Gdx.input.isKeyJustPressed(InputGBA.ButtonDOWN))
        {
            newPos.y--;
        }
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonLEFT))
        {
            newPos.x--;
            lookingLeft = true;
        }
        else if (Gdx.input.isKeyJustPressed(InputGBA.ButtonRIGHT))
        {
            newPos.x++;
            lookingLeft = false;
        }
        if (newPos.x > -1 && newPos.x < 240 / 16f && newPos.y > -1 && newPos.y < 160 / 16f - 1)
        {
            if (map[(int) newPos.x][(int) newPos.y] == 0)
            {
                playerPos = new Vector2(newPos.x, newPos.y);
            }
        }
        offset = MathUtils.lerp(offset, (lookingLeft ? -1 : 1), delta);

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        ufl.DrawString(batch, new Vector2(playerPos.x * 16, (playerPos.y + 1) * 16), "⛄", Color.WHITE, true);
        for (int z = 0; z < 16; z++)
        {
            for (int x = 0; x < map.length; x++)
            {
                for (int y = 0; y < map[0].length; y++)
                {
                    if (map[x][y] != 0)
                    {
                        Color c = Color.WHITE;
                        float t = (z + 1);
                        c = new Color(1 / 16f * t, 1 / 16f * t, 1 / 16f * t, 1f);
                        platformLayers[z].setPosition((x * 16) - (z * offset / 2f), y * 16);
                        platformLayers[z].setColor(c);
                        platformLayers[z].draw(batch);
                    }
                }
            }
        }
        DecimalFormat df = new DecimalFormat(" #.##;-#.##");
        ufl.DrawString(batch, new Vector2(0, 160), "3D Offset: " + df.format(offset) + "\n" + "Looking Left: " + (lookingLeft), Color.WHITE, 0.5f);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        Gdx.graphics.setWindowedMode(240, 160);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
