package com.poc.game.screens.spriteStacking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

import java.text.DecimalFormat;

public class SpriteStackingPerspectiveTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private TextureAtlas cubeTexture;
    private Sprite[] cubeLayers;
    private TextureAtlas pyramidTexture;
    private Sprite[] pyramidLayers;
    private float perspective;
    byte[][][] map;
    Vector3 playerPos;

    private POC root;

    @Override
    public String toString()
    {
        return "[WIP] Sprite Stacking Perspective";
    }

    public SpriteStackingPerspectiveTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        Texture texture[] = {new Texture("cube.png"), new Texture("pyramid.png")};
        cubeTexture = new TextureAtlas();
        cubeLayers = new Sprite[16];
        pyramidTexture = new TextureAtlas();
        pyramidLayers = new Sprite[16];
        for (int i = 0; i < 16; i++)
        {
            cubeTexture.addRegion(String.valueOf(i), texture[0], 0, (16 * 16) - 16 - (16 * i), 16, 16);
            cubeLayers[i] = cubeTexture.createSprite(String.valueOf(i));
            pyramidTexture.addRegion(String.valueOf(i), texture[1], 0, (16 * 16) - 16 - (16 * i), 16, 16);
            pyramidLayers[i] = pyramidTexture.createSprite(String.valueOf(i));
        }
        perspective = 0;
        map = new byte[][][]
                {
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 2, 0, 0, 0, 0, 0, 0, 2, 0},
                                {0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 2, 0, 0, 0, 0, 0, 0, 2, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        }
                };
        playerPos = new Vector3(Math.round((240 / 2f) / 16f), Math.round((160 / 2f) / 16f), 0);

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }
        Vector3 newPos = new Vector3(playerPos.x, playerPos.y, playerPos.z);
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonUP))
        {
            newPos.y++;
        }
        else if (Gdx.input.isKeyJustPressed(InputGBA.ButtonDOWN))
        {
            newPos.y--;
        }
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonLEFT))
        {
            newPos.x--;
        }
        else if (Gdx.input.isKeyJustPressed(InputGBA.ButtonRIGHT))
        {
            newPos.x++;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.I))
        {
            perspective = 0f;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.K))
        {
            perspective = 3.14f;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.J))
        {
            perspective = 270f / 180f * 3.14f;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.L))
        {
            perspective = 90f / 180f * 3.14f;
        }
        if (newPos.x > -1 && newPos.x < 240 / 16f - 1 && newPos.y > -1 && newPos.y < 160 / 16f - 1)
        {
            if (newPos.z < map.length)
            {
                if (map[(int) newPos.z][(int) newPos.x][(int) newPos.y] == 0 && map[(int) newPos.z + 1][(int) newPos.x][(int) newPos.y] == 0)
                {
                    playerPos = new Vector3(newPos.x, newPos.y, newPos.z);
                }
                if (map[(int) newPos.z][(int) newPos.x][(int) newPos.y] == 1 && map[(int) newPos.z + 1][(int) newPos.x][(int) newPos.y] == 0)
                {
                    newPos.z++;
                    playerPos = new Vector3(newPos.x, newPos.y, newPos.z);
                }
            }
            if (newPos.z > 0)
            {
                if (map[(int) newPos.z][(int) newPos.x][(int) newPos.y] == 0 && map[(int) newPos.z - 1][(int) newPos.x][(int) newPos.y] == 0)
                {
                    newPos.z--;
                    playerPos = new Vector3(newPos.x, newPos.y, newPos.z);
                }
            }
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        for (int x = 0; x < map[0].length; x++)
        {
            for (int y = map[0][0].length - 1; y > 0; y--)
            {
                for (int z = 0; z < map.length; z++)
                {
                    if (map[z][x][y] != 0)
                    {
                        Color c = Color.WHITE;
                        if (z > playerPos.z + 1)
                        {
                            continue;
                        }
                        else if (z == playerPos.z + 1)
                        {
                            c = new Color(1, 1, 1, 0.75f);
                        }
                        else if (z < playerPos.z)
                        {
                            float k = (float) Math.pow(0.5f, (playerPos.z - z));
                            c = new Color(1 * k, 1 * k, 1 * k, 1);
                        }

                        Vector2 rotOffsetBase = new Vector2(x * 16, (y * 16) + (16 * z) - (16 * playerPos.z));
                        Vector2 rotOffsetTop = new Vector2(x * 16, (y * 16) + (16 * z) + 1 - (16 * playerPos.z));
                        switch (map[z][x][y])
                        {
                            case 1:
                                for (int i = 0; i < cubeLayers.length; i++)
                                {
                                    cubeLayers[i].setColor(new Color(c.r * 0.75f, c.g * 0.75f, c.b * 0.75f, c.a));
                                    cubeLayers[i].setPosition(rotOffsetBase.x + (float) Math.sin(perspective) * i, rotOffsetBase.y + i * (float) Math.cos(perspective));
                                    cubeLayers[i].draw(batch);
                                    cubeLayers[i].setColor(c);
                                    cubeLayers[i].setPosition(rotOffsetTop.x + (float) Math.sin(perspective) * i, rotOffsetTop.y + i * (float) Math.cos(perspective));
                                    cubeLayers[i].draw(batch);
                                }
                                break;
                            case 2:
                                for (int i = 0; i < pyramidLayers.length; i++)
                                {
                                    pyramidLayers[i].setColor(new Color(c.r * 0.75f, c.g * 0.75f, c.b * 0.75f, c.a));
                                    pyramidLayers[i].setPosition(rotOffsetBase.x + (float) Math.sin(perspective) * i, rotOffsetBase.y + i * (float) Math.cos(perspective));
                                    pyramidLayers[i].draw(batch);
                                    pyramidLayers[i].setColor(c);
                                    pyramidLayers[i].setPosition(rotOffsetTop.x + (float) Math.sin(perspective) * i, rotOffsetTop.y + i * (float) Math.cos(perspective));
                                    pyramidLayers[i].draw(batch);
                                }
                                break;
                        }
                    }
                }
            }
        }
        ufl.DrawString(batch, new Vector2(playerPos.x * 16, (playerPos.y + 1) * 16), "⛄", Color.WHITE, true);
        ufl.DrawString(batch, new Vector2(0, 160), "⃝\n⃤\n⃞\n" + (float) Math.sin(perspective) + ", " + (float) Math.cos(perspective), Color.WHITE, 0.5f, true);
        DecimalFormat df = new DecimalFormat(" #;-#");
        ufl.DrawString(batch, new Vector2(8, 160), df.format(playerPos.z + 1) + "\n" + df.format(playerPos.z) + "\n" + df.format(playerPos.z - 1), Color.WHITE, 0.5f);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
