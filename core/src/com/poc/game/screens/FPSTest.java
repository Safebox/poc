package com.poc.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

import java.util.Calendar;

public class FPSTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private POC root;

    @Override
    public String toString()
    {
        return "FPS Test";
    }

    public FPSTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        if (Math.round(Calendar.getInstance().getTimeInMillis() / 100.0) % 10.0 == 0)
        {
            Gdx.gl.glClearColor(1, 1, 1, 1);
        }
        else
        {
            Gdx.gl.glClearColor(0, 0, 0, 1);
        }
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        if (Math.round(Calendar.getInstance().getTimeInMillis() / 100.0) % 10.0 != 0)
        {
            ufl.DrawString(batch, new Vector2(0, 160), String.valueOf(Calendar.getInstance().getTimeInMillis()), Color.WHITE);
            ufl.DrawString(batch, new Vector2(0, 160 - 16), String.valueOf((long)(Math.round(Calendar.getInstance().getTimeInMillis() / 100.0) % 10.0)), Color.WHITE);
        }
        else
        {
            ufl.DrawString(batch, new Vector2(0, 160), String.valueOf(Calendar.getInstance().getTimeInMillis()), Color.BLACK);
            ufl.DrawString(batch, new Vector2(0, 160 - 16), String.valueOf((long)(Math.round(Calendar.getInstance().getTimeInMillis() / 100.0) % 10.0)), Color.BLACK);
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
