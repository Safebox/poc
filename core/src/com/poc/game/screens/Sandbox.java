package com.poc.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

public class Sandbox implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private POC root;

    @Override
    public String toString()
    {
        return "Sandbox";
    }

    public Sandbox(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        ufl.DrawString(batch, new Vector2(0, 160), "また「ボーンチャーム」と呼ばれ\n" +
                "る簡易版のルーンを入手すること\n" +
                "ができ、それを身につけると力の\n" +
                "一部を任意に強化することができ\n" +
                "ます。ボーンチャームは全部で約\n" +
                "50種類あり、1回のゲームプレ\n" +
                "イでは20種類ほどしか入手でき\n" +
                "ず（取得効果はランダムに発生す\n" +
                "るため）リプレイ性を高めるやり\n" +
                "込み要素にもなっています。", Color.WHITE, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
