# Autotile
## Description
In tile-based games, there is a technique known as autotiling (RPG Maker), connecting textures (Minecraft) or marching squares (almost every other occurance). This allows efficient use of a 5-6 til texture which can be rendered in up to 256 different ways to give the appearance of a naturally connecting map. This allows maps to be designed faster and with less storage as only the IDs of the different tile types need to be stored instead of the individual tiles themselves. It should be noted that there is a 3D variation called marching cubes, but for most tile-based scenarios marching squares can be expanded to suit the requirements.
## Process
**Check Neighbours**
1) check neighbours for tiles of the same ID
2) for matches, add their corresponding index to a byte variable

**Draw Quadrants**
3) for each quadrant, bitmask the byte variable to get its neighbours
4) determine whether to draw a top-down, left-right, outward-corner, inward-corner or fill tile