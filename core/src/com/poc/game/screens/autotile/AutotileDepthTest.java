package com.poc.game.screens.autotile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

import java.text.DecimalFormat;

public class AutotileDepthTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private Texture autotile;
    private Autotile at;
    byte[][][] map;
    Vector3 playerPos;

    private POC root;

    @Override
    public String toString()
    {
        return "Autotile Depth";
    }

    public AutotileDepthTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        autotile = new Texture("AutotileTest.png");
        at = new Autotile((byte) 1, autotile);
        map = new byte[][][]
                {
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 1, 0, 0, 1, 0, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 1, 0, 0, 1, 0, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 1, 0, 0, 1, 0, 1, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        },
                        {
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                        }
                };
        playerPos = new Vector3(Math.round((240 / 2f) / 16f),  Math.round((160 / 2f) / 16f), 0);

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }
        Vector3 newPos = new Vector3(playerPos.x, playerPos.y, playerPos.z);
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonUP))
        {
            newPos.y++;
        }
        else if (Gdx.input.isKeyJustPressed(InputGBA.ButtonDOWN))
        {
            newPos.y--;
        }
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonLEFT))
        {
            newPos.x--;
        } else if (Gdx.input.isKeyJustPressed(InputGBA.ButtonRIGHT))
        {
            newPos.x++;
        }
        if (newPos.x > -1 && newPos.x < 240 / 16f - 1 && newPos.y > -1 && newPos.y < 160 / 16f - 1)
        {
            if (newPos.z < map.length)
            {
                if (map[(int) newPos.z][(int) newPos.x][(int) newPos.y] == 0 && map[(int) newPos.z + 1][(int) newPos.x][(int) newPos.y] == 0)
                {
                    playerPos = new Vector3(newPos.x, newPos.y, newPos.z);
                }
                if (map[(int) newPos.z][(int) newPos.x][(int) newPos.y] == 1 && map[(int) newPos.z + 1][(int) newPos.x][(int) newPos.y] == 0)
                {
                    newPos.z++;
                    playerPos = new Vector3(newPos.x, newPos.y, newPos.z);
                }
            }
            if (newPos.z > 0)
            {
                if (map[(int) newPos.z][(int) newPos.x][(int) newPos.y] == 0 && map[(int) newPos.z - 1][(int) newPos.x][(int) newPos.y] == 0)
                {
                    newPos.z--;
                    playerPos = new Vector3(newPos.x, newPos.y, newPos.z);
                }
            }
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        for (int x = 0; x < map[0].length; x++)
        {
            for (int y = 0; y < map[0][0].length; y++)
            {
                for (int z = 0; z < map.length; z++)
                {
                    if (map[z][x][y] != 0)
                    {
                        Color c = Color.WHITE;
                        if (z > playerPos.z + 1)
                        {
                            continue;
                        }
                        else if (z == playerPos.z + 1)
                        {
                            c = new Color(1, 1, 1, 0.75f);
                        }
                        else if (z < playerPos.z)
                        {
                            float k = (float) Math.pow(0.5f, (playerPos.z - z));
                            c = new Color(1 * k, 1 * k, 1 * k, 1);
                        }
                        at.RenderTile(batch, map[z], new Vector2(x, y), c);
                    }
                }
            }
        }
        ufl.DrawString(batch, new Vector2(playerPos.x * 16, (playerPos.y + 1) * 16), "⛄", Color.WHITE, true);
        ufl.DrawString(batch, new Vector2(0, 160), "⃝\n⃤\n⃞", Color.WHITE, 0.5f, true);
        DecimalFormat df = new DecimalFormat(" #;-#");
        ufl.DrawString(batch, new Vector2(8, 160), df.format(playerPos.z + 1) + "\n" + df.format(playerPos.z) + "\n" + df.format(playerPos.z - 1), Color.WHITE, 0.5f);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        Gdx.graphics.setWindowedMode(240, 160);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
