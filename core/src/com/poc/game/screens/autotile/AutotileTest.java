package com.poc.game.screens.autotile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.poc.game.POC;
import utilities.InputGBA;
import utilities.UnifontLoader;

public class AutotileTest implements Screen
{
    private SpriteBatch batch;
    private UnifontLoader ufl;

    private Texture autotile;
    private Autotile at;
    byte[][] map;

    private POC root;

    @Override
    public String toString()
    {
        return "Autotile";
    }

    public AutotileTest(POC root)
    {
        batch = new SpriteBatch();
        ufl = new UnifontLoader();

        autotile = new Texture("AutotileTest.png");
        at = new Autotile((byte) 1, autotile);
        map = new byte[][]
                {
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                        {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1},
                        {0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
                        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                        {1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
                        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1},
                        {0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
                        {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
                };

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(InputGBA.ButtonESC))
        {
            root.SetScreen(0);
            return;
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(root.cam.combined);
        batch.begin();
        for (int x = 0; x < map.length; x++)
        {
            for (int y = 0; y < map[0].length; y++)
            {
                if (map[x][y] != 0)
                {
                    at.RenderTile(batch, map, new Vector2(x, y), Color.WHITE);
                }
            }
        }
        //ufl.DrawString(batch, new Vector2(8, 160 - 8)/*view.getWorldHeight() - 1 - 8)*/, "Roman, ελληνικά, ϯⲙⲉⲧⲣⲉⲙⲛ̀ⲭⲏⲙⲓ / ⲧⲙⲛ̄ⲧⲣⲙ̄ⲛ̄ⲕⲏⲙⲉ, Кириллица", Color.WHITE, 0.5f);
        //ufl.DrawString(batch, new Vector2(8, 160 - 16)/*view.getWorldHeight() - 1 - 8)*/, "հայերէն / հայերեն, देवनागरी", Color.WHITE, 0.5f, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height)
    {
        Gdx.gl.glViewport(Gdx.graphics.getWidth() / 2 - (int)root.view.getWorldWidth() / 2, Gdx.graphics.getHeight() / 2 - (int)root.view.getWorldHeight() / 2, (int)root.view.getWorldWidth(), (int)root.view.getWorldHeight());
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
