package com.poc.game.screens.autotile;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Autotile
{
    //Variables
    private byte tileID;
    private Texture tex2D;
    private int quadrantSize;

    //Properties
    public byte TileID()
    {
        final byte t = tileID;
        return t;
    }

    public Texture Texture()
    {
        final Texture t = tex2D;
        return t;
    }

    public int QuadrantSize()
    {
        final int s = quadrantSize;
        return s;
    }

    //Constructors
    public Autotile(byte id, Texture texture)
    {
        tileID = id;
        tex2D = texture;
        quadrantSize = texture.getWidth() / 4;
    }

    //Methods
    /*
     * 128 1 2
     * 64    4
     * 32 16 8
     *
     * 128-64-32-16-8-4-2-1
     */
    private short UpdateMask(byte[][] tilemap, Vector2 position)
    {
        short mask = 0;
        for (int a = 0; a < 8; a++)
        {
            Vector2 offsetPos = new Vector2((float) Math.round(position.x + Math.sin(a / 4f * 3.14f)), (float) Math.round(position.y + Math.cos(a / 4f * 3.14f)));
            if (offsetPos.x > -1 && offsetPos.y > -1 && offsetPos.x < tilemap.length && offsetPos.y < tilemap[0].length)
            {
                if (tilemap[(int) Math.round(offsetPos.x)][(int) Math.round(offsetPos.y)] == tileID)
                {
                    mask += (short) Math.pow(2, a);
                }
            }
        }
        return mask;
    }

    private Rectangle[] GetQuadrants(byte[][] tilemap, Vector2 position)
    {
        short mask = UpdateMask(tilemap, position);
        return new Rectangle[]{TL(mask), TR(mask), BL(mask), BR(mask)};
    }

    private Rectangle TL(short mask)
    {
        short[] edgeCheckTB = {1, 1 + 128};
        short[] edgeCheckLR = {64, 64 + 128};
        short cornerCheckInt = 1 + 64;
        short fillCheck = 1 + 64 + 128;

        short newMask = (short) (mask & fillCheck);

        if (newMask == edgeCheckTB[0] || newMask == edgeCheckTB[1])
        {
            return new Rectangle(0, quadrantSize * 4, quadrantSize, quadrantSize);
        }
        else if (newMask == edgeCheckLR[0] || newMask == edgeCheckLR[1])
        {
            return new Rectangle(quadrantSize * 2, quadrantSize * 2, quadrantSize, quadrantSize);
        }
        else if (newMask == cornerCheckInt)
        {
            return new Rectangle(quadrantSize * 2, 0, quadrantSize, quadrantSize);
        }
        else if (newMask == fillCheck)
        {
            return new Rectangle(quadrantSize * 2, quadrantSize * 4, quadrantSize, quadrantSize);
        }
        return new Rectangle(0, quadrantSize * 2, quadrantSize, quadrantSize);
    }

    private Rectangle TR(short mask)
    {
        short[] edgeCheckTB = {1, 1 + 2};
        short[] edgeCheckLR = {4, 2 + 4};
        short cornerCheckInt = 1 + 4;
        short fillCheck = 1 + 2 + 4;

        short newMask = (short) (mask & fillCheck);

        if (newMask == edgeCheckTB[0] || newMask == edgeCheckTB[1])
        {
            return new Rectangle(quadrantSize * 3, quadrantSize * 4, quadrantSize, quadrantSize);
        }
        else if (newMask == edgeCheckLR[0] || newMask == edgeCheckLR[1])
        {
            return new Rectangle(quadrantSize, quadrantSize * 2, quadrantSize, quadrantSize);
        }
        else if (newMask == cornerCheckInt)
        {
            return new Rectangle(quadrantSize * 3, 0, quadrantSize, quadrantSize);
        }
        else if (newMask == fillCheck)
        {
            return new Rectangle(quadrantSize, quadrantSize * 4, quadrantSize, quadrantSize);
        }
        return new Rectangle(quadrantSize * 3, quadrantSize * 2, quadrantSize, quadrantSize);
    }

    private Rectangle BL(short mask)
    {
        short[] edgeCheckTB = {16, 16 + 32};
        short[] edgeCheckLR = {64, 32 + 64};
        short cornerCheckInt = 16 + 64;
        short fillCheck = 16 + 32 + 64;

        short newMask = (short) (mask & fillCheck);

        if (newMask == edgeCheckTB[0] || newMask == edgeCheckTB[1])
        {
            return new Rectangle(0, quadrantSize * 3, quadrantSize, quadrantSize);
        }
        else if (newMask == edgeCheckLR[0] || newMask == edgeCheckLR[1])
        {
            return new Rectangle(quadrantSize * 2, quadrantSize * 5, quadrantSize, quadrantSize);
        }
        else if (newMask == cornerCheckInt)
        {
            return new Rectangle(quadrantSize * 2, quadrantSize, quadrantSize, quadrantSize);
        }
        else if (newMask == fillCheck)
        {
            return new Rectangle(quadrantSize * 2, quadrantSize * 3, quadrantSize, quadrantSize);
        }
        return new Rectangle(0, quadrantSize * 5, quadrantSize, quadrantSize);
    }

    private Rectangle BR(short mask)
    {
        short[] edgeCheckTB = {16, 8 + 16};
        short[] edgeCheckLR = {4, 4 + 8};
        short cornerCheckInt = 4 + 16;
        short fillCheck = 4 + 8 + 16;

        short newMask = (short) (mask & fillCheck);

        if (newMask == edgeCheckTB[0] || newMask == edgeCheckTB[1])
        {
            return new Rectangle(quadrantSize * 3, quadrantSize * 3, quadrantSize, quadrantSize);
        }
        else if (newMask == edgeCheckLR[0] || newMask == edgeCheckLR[1])
        {
            return new Rectangle(quadrantSize, quadrantSize * 5, quadrantSize, quadrantSize);
        }
        else if (newMask == cornerCheckInt)
        {
            return new Rectangle(quadrantSize * 3, quadrantSize, quadrantSize, quadrantSize);
        }
        else if (newMask == fillCheck)
        {
            return new Rectangle(quadrantSize, quadrantSize * 3, quadrantSize, quadrantSize);
        }
        return new Rectangle(quadrantSize * 3, quadrantSize * 5, quadrantSize, quadrantSize);
    }

    public void RenderTile(SpriteBatch spriteBatch, byte[][] tilemap, Vector2 position, Color color)
    {
        Rectangle[] sourceRectangle = GetQuadrants(tilemap, position);
        spriteBatch.setColor(color);

        spriteBatch.draw(Texture(), position.x * 16, (position.y + 0.5f) * 16, (int) sourceRectangle[0].x, (int) sourceRectangle[0].y, (int) sourceRectangle[0].getWidth(), (int) sourceRectangle[0].getHeight());
        spriteBatch.draw(Texture(), position.x * 16 + 8, (position.y + 0.5f) * 16, (int) sourceRectangle[1].x, (int) sourceRectangle[1].y, (int) sourceRectangle[1].getWidth(), (int) sourceRectangle[1].getHeight());
        spriteBatch.draw(Texture(), position.x * 16, (position.y + 0.5f) * 16 - 8, (int) sourceRectangle[2].x, (int) sourceRectangle[2].y, (int) sourceRectangle[2].getWidth(), (int) sourceRectangle[2].getHeight());
        spriteBatch.draw(Texture(), position.x * 16 + 8, (position.y + 0.5f) * 16 - 8, (int) sourceRectangle[3].x, (int) sourceRectangle[3].y, (int) sourceRectangle[3].getWidth(), (int) sourceRectangle[3].getHeight());

        spriteBatch.setColor(Color.WHITE);
    }
}