# Autotile Depth
## Description
In traditional roguelike games, it is difficult to represent depth both in terms of terrain height and relative to the player as a whole. Some ASCII roguelikes use arrows or numbers on a minimap to represent the height of the terrain around the player while other omit a height system altogether, going for a floor-based approach instead. Dwarf Fortress and The Escapists implemented this floor-based approach with the latter notably having all floors below the current one visible at the same time. Following on from this implementation, this prototype is to check the feasibility of a top-down 2D world with depth at the scale of Minecraft, where the player is two tiles high.
## Process
1) if tile is on same depth as player, render it
2) if tile is one above depth of player, render it at 75% opacity
3) if tile is below depth of player, render it at (0.5 ^ (distance from depth of player))