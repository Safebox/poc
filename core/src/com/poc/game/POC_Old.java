package com.poc.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import utilities.UnifontLoader;

public class POC_Old extends ApplicationAdapter
{
    SpriteBatch batch;
    Texture img;

    UnifontLoader ufl;

    @Override
    public void create()
    {
        batch = new SpriteBatch();
        img = new Texture("badlogic.jpg");

        ufl = new UnifontLoader();
    }

    @Override
    public void render()
    {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        //batch.draw(img, 0, 0);
        ufl.DrawString(batch, new Vector2(16,Gdx.graphics.getHeight() - 1 - 16), "Hello World!", Color.WHITE);
        ufl.DrawString(batch, new Vector2(16,Gdx.graphics.getHeight() - 1 - 32), "          せ かい", Color.WHITE, 0.5f, true);
        ufl.DrawString(batch, new Vector2(16,Gdx.graphics.getHeight() - 1 - 40), "こんにちは世界", Color.WHITE,true);

        ufl.DrawString(batch, new Vector2(8,Gdx.graphics.getHeight() - 1 - 8), String.valueOf(Gdx.graphics.getFramesPerSecond()) + "fps (rounded) = " + String.valueOf(Math.round(1f / Gdx.graphics.getRawDeltaTime() * 100) / 100f) + "fps (exact)", Color.WHITE, 0.5f);
        batch.end();
    }

    @Override
    public void dispose()
    {
        batch.dispose();
        img.dispose();
    }
}