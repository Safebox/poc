package utilities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Require's GNU Unifont's Plane 0 cropped to 4096 x 4096 as a base. Available at: http://unifoundry.com/unifont/index.html
 */
public class UnifontLoaderV2
{
    //Variables
    Texture texture;
    List<Vector2> characterList;
    List<Integer> diacriticList;

    //Properties
    public Texture getTexture()
    {
        return texture;
    }

    //Constructors
    public UnifontLoaderV2()
    {
        texture = new Texture("Unifont.png");
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        characterList = new ArrayList<Vector2>();
        diacriticList = new ArrayList<Integer>();
        for (int c = 0; c < (texture.getWidth() / 16) * (texture.getHeight() / 16); c++)
        {
            characterList.add(new Vector2((int) ((c * 16f) % (texture.getWidth())), (int) ((c * 16f) / (texture.getWidth())) * 16));
            if (Character.getType((char) c) == Character.NON_SPACING_MARK || Character.getType((char) c) == Character.COMBINING_SPACING_MARK)
            {
                diacriticList.add(c);
            }
        }
    }

    //Methods
    public void drawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color)
    {
        drawString(spriteBatch, position, text, color, 1f, false);
    }

    /**
     * @param scale 1 = 8 x 16
     */
    public void drawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, float scale)
    {
        drawString(spriteBatch, position, text, color, scale, false);
    }

    public void drawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, boolean fullwidth)
    {
        drawString(spriteBatch, position, text, color, 1f, fullwidth);
    }

    /**
     * @param scale 1 = (fullwidth ? 16 : 8) x 16
     */
    public void drawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, float scale, boolean fullwidth)
    {
        Pattern pattern = Pattern.compile("\\*(.+)\\*|\\/(.+)\\/|_(.+)_|(\\n)|([^\\/*\\n_]+)");
        Vector2 posOffset = new Vector2();
        boolean firstGlyphOfLine = true;

        Matcher matcher = pattern.matcher(text);
        while (matcher.find())
        {
            if (matcher.group(1) != null)
            {
                String token = matcher.group(1);
                if (scale % 1 == 0)
                {
                    for (int i = 0; i < token.length(); i++)
                    {
                        firstGlyphOfLine = isFirstLine_isDiacritic_incrementCursor(posOffset, token.charAt(i), scale, fullwidth, firstGlyphOfLine);

                        drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x, position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                        drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x + scale, position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                    }
                }
                else
                {
                    for (int i = 0; i < token.length(); i++)
                    {
                        firstGlyphOfLine = isFirstLine_isDiacritic_incrementCursor(posOffset, token.charAt(i), scale, fullwidth, firstGlyphOfLine);

                        drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x, position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                        drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x, position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                    }
                }
            }
            else if (matcher.group(2) != null)
            {
                String token = matcher.group(2);
                spriteBatch.end();
                spriteBatch.setTransformMatrix(new Matrix4().setAsAffine(new Affine2().shear(0.25f, 0f)));
                spriteBatch.begin();
                for (int i = 0; i < token.length(); i++)
                {
                    firstGlyphOfLine = isFirstLine_isDiacritic_incrementCursor(posOffset, token.charAt(i), scale, fullwidth, firstGlyphOfLine);

                    drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x - (0.25f * (position.y + posOffset.y)) + ((fullwidth ? 8f : 4f) * 0.25f), position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                }
                spriteBatch.end();
                spriteBatch.setTransformMatrix(new Matrix4());
                spriteBatch.begin();
            }
            else if (matcher.group(3) != null)
            {
                String token = matcher.group(3);
                for (int i = 0; i < token.length(); i++)
                {
                    firstGlyphOfLine = isFirstLine_isDiacritic_incrementCursor(posOffset, token.charAt(i), scale, fullwidth, firstGlyphOfLine);

                    drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x, position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                    drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x, position.y + posOffset.y), charFinder((char) 818), color, scale);
                    if (fullwidth)
                    {
                        drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x + (8f * scale), position.y + posOffset.y), charFinder((char) 818), color, scale);
                    }
                }
            }
            else if (matcher.group(4) != null)
            {
                posOffset.x = 0;
                posOffset.y -= 16f * scale;
                firstGlyphOfLine = true;
            }
            else if (matcher.group(5) != null)
            {
                String token = matcher.group(5).replaceAll("\t", (fullwidth ? "  " : "    "));
                for (int i = 0; i < token.length(); i++)
                {
                    firstGlyphOfLine = isFirstLine_isDiacritic_incrementCursor(posOffset, token.charAt(i), scale, fullwidth, firstGlyphOfLine);

                    drawCharacter(spriteBatch, new Vector2(position.x + posOffset.x, position.y + posOffset.y), charFinder(token.charAt(i)), color, scale);
                }
            }
        }
    }

    private boolean isFirstLine_isDiacritic_incrementCursor(Vector2 posOffset, char token, float scale, boolean fullwidth, boolean firstGlyphOfLine)
    {
        if (firstGlyphOfLine)
        {
            posOffset.x = 0;
        }
        else
        {
            boolean isDiacritic = false;
            for (int d : diacriticList)
            {
                if (token == d)
                {
                    isDiacritic = true;
                    break;
                }
            }
            if (!isDiacritic)
            {
                incrementCursor(posOffset, scale, fullwidth);
            }
        }
        return false;
    }

    private Rectangle charFinder(char c)
    {
        return new Rectangle(characterList.get(c).x, characterList.get(c).y, 16, 16);
    }

    private void incrementCursor(Vector2 posOffset, float scale, boolean fullwidth)
    {
        posOffset.x += 16f * scale * (fullwidth ? 1f : 0.5f);
    }

    private void drawCharacter(SpriteBatch spriteBatch, Vector2 pos, Rectangle charRect, Color color, float scale)
    {
        for (int j = 0; j < 1f / scale; j++)
        {
            spriteBatch.setColor(color);
            spriteBatch.draw(texture, pos.x, pos.y, charRect.getWidth() * scale, -charRect.getHeight() * scale, charRect.x / texture.getWidth(), charRect.y / texture.getHeight(), (charRect.x + 16) / texture.getWidth(), (charRect.y + 16) / texture.getHeight());
            spriteBatch.setColor(Color.WHITE);
        }
    }
}