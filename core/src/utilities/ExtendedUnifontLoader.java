package utilities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/// <summary>
/// Require's GNU Unifont's Plane 0 cropped to 4096 x 4096 as a base. Available at: http://unifoundry.com/unifont/index.html
/// </summary>
public class ExtendedUnifontLoader
{
    //Variables
    Texture texture;
    Vector2[] characterList;
    int[] diacriticList;
    ArrayList<Float> typewriterOffsets;

    //Properties
    public Texture Texture()
    {
        return texture;
    }

    public ExtendedUnifontLoader()
    {
        texture = new Texture("Unifont.png");
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        characterList = new Vector2[(texture.getWidth() / 16) * (texture.getHeight() / 16)];
        diacriticList = new int[1247];
        int d = 0;
        for (int c = 0; c < characterList.length; c++)
        {
            characterList[c] = new Vector2((int) ((c * 16f) % (texture.getWidth())), (int) ((c * 16f) / (texture.getWidth())) * 16);
            if (Character.getType((char) c) == Character.NON_SPACING_MARK || Character.getType((char) c) == Character.COMBINING_SPACING_MARK)
            {
                diacriticList[d] = c;
                d++;
            }
        }
        typewriterOffsets = new ArrayList<Float>();
    }

    //Methods
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, int typewriterIndex, Color color)
    {
        DrawString(spriteBatch, position, text, typewriterIndex, color, 1f, false);
    }

    /// <param name="scale">1 = 16 x 16</param>
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, int typewriterIndex, Color color, float scale)
    {
        DrawString(spriteBatch, position, text, typewriterIndex, color, scale, false);
    }

    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, int typewriterIndex, Color color, boolean fullwidth)
    {
        DrawString(spriteBatch, position, text, typewriterIndex, color, 1f, fullwidth);
    }

    /// <param name="scale">1 = 16 x 16</param>
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, int typewriterIndex, Color color, float scale, boolean fullwidth)
    {
        Vector2 posOffset = new Vector2();
        boolean firstGlyphOfLine = true;

        for (int i = 0; i < (int)Math.min(Math.floor(typewriterOffsets.get(typewriterIndex)), text.length()); i++)
        {
            char c = text.charAt(i);
            if (c == '\n')
            {
                posOffset.x = 0;
                posOffset.y -= 16f * scale;
                firstGlyphOfLine = true;
            }
            else if (c == '\t')
            {
                position.x += (16f * 3f) * scale * (fullwidth ? 1f : 0.5f);
            }
            else
            {
                if (firstGlyphOfLine)
                {
                    posOffset.x = 0;
                    firstGlyphOfLine = false;
                }
                else
                {
                    boolean isDiacritic = false;
                    for (int d : diacriticList)
                    {
                        if (c == d)
                        {
                            isDiacritic = true;
                            break;
                        }
                    }
                    if (!isDiacritic)
                    {
                        posOffset.x += 16f * scale * (fullwidth ? 1f : 0.5f);
                    }
                }

                if (c != '\r' && c != '\n')
                {
                    Rectangle charRect = CharFinder(c);
                    Vector2 pos = new Vector2(position.x + posOffset.x, position.y + posOffset.y);
                    for (int j = 0; j < 1f / scale; j++)
                    {
                        spriteBatch.setColor(color);
                        spriteBatch.draw(texture, pos.x, pos.y, charRect.getWidth() * scale, -charRect.getHeight() * scale, charRect.x / texture.getWidth(), charRect.y / texture.getHeight(), (charRect.x + 16) / texture.getWidth(), (charRect.y + 16) / texture.getHeight());
                        spriteBatch.setColor(Color.WHITE);
                    }
                }
            }
        }
    }

    public void AddTypewriter(int index)
    {
        typewriterOffsets.add(index, 0f);
    }

    public float GetTypewriter(int index)
    {
        return typewriterOffsets.get(index);
    }

    public void IncrementTypewriter(int index, float offset)
    {
        typewriterOffsets.set(index, typewriterOffsets.get(index) + offset);
    }

    public void ResetTypewriter(int index)
    {
        typewriterOffsets.set(index, 0f);
    }

    public void RemoveTypewriter(int index)
    {
        typewriterOffsets.remove(index);
    }

    private Rectangle CharFinder(char c)
    {
        return new Rectangle(characterList[c].x, characterList[c].y, 16, 16);
    }
}