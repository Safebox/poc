package utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;

public class CustomfontLoader
{
    //Variables
    Texture texture;
    HashMap<Integer, Vector2> characterList;
    int[] diacriticList;

    //Properties
    public Texture Texture()
    {
        return texture;
    }

    public CustomfontLoader(String textureFile, String characterFile)
    {
        texture = new Texture(textureFile);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        characterList = new HashMap<Integer, Vector2>();
        diacriticList = new int[1247];
        String characters = Gdx.files.internal(characterFile).readString("UTF-16");
        characters = characters.replace("\n","").replace("\r","");
        int d = 0;
        for (int i = 0; i < characters.length(); i++)
        {
            int c = characters.charAt(i);
            characterList.put(c, new Vector2((int) ((i * 16f) % (texture.getWidth())), (int) ((i * 16f) / (texture.getWidth())) * 16));
            if (Character.getType((char) c) == Character.NON_SPACING_MARK || Character.getType((char) c) == Character.COMBINING_SPACING_MARK)
            {
                diacriticList[d] = c;
                d++;
            }
        }
    }

    //Methods
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color)
    {
        DrawString(spriteBatch, position, text, color, 1f, false);
    }

    /// <param name="scale">1 = 16 x 16</param>
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, float scale)
    {
        DrawString(spriteBatch, position, text, color, scale, false);
    }

    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, boolean fullwidth)
    {
        DrawString(spriteBatch, position, text, color, 1f, fullwidth);
    }

    /// <param name="scale">1 = 16 x 16</param>
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, float scale, boolean fullwidth)
    {
        Vector2 posOffset = new Vector2();
        boolean firstGlyphOfLine = true;

        for (int i = 0; i < text.length(); i++)
        {
            if (text.charAt(i) == '\n')
            {
                posOffset.x = 0;
                posOffset.y -= 16f * scale;
                firstGlyphOfLine = true;
            }
            else if (text.charAt(i) == '\t')
            {
                position.x += (16f * 3f) * scale * (fullwidth ? 1f : 0.5f);
            }
            else
            {
                if (firstGlyphOfLine)
                {
                    posOffset.x = 0;
                    firstGlyphOfLine = false;
                }
                else
                {
                    boolean isDiacritic = false;
                    for (int d : diacriticList)
                    {
                        if (text.charAt(i) == d)
                        {
                            isDiacritic = true;
                            break;
                        }
                    }
                    if (!isDiacritic)
                    {
                        posOffset.x += 16f * scale * (fullwidth ? 1f : 0.5f);
                    }
                }

                if (text.charAt(i) != '\r' && text.charAt(i) != '\n')
                {
                    Rectangle charRect = CharFinder(text.charAt(i));
                    Vector2 pos = new Vector2(position.x + posOffset.x, position.y + posOffset.y);
                    for (int j = 0; j < 1f / scale; j++)
                    {
                        spriteBatch.setColor(color);
                        spriteBatch.draw(texture, pos.x, pos.y, charRect.getWidth() * scale, -charRect.getHeight() * scale, charRect.x / texture.getWidth(), charRect.y / texture.getHeight(), (charRect.x + 16) / texture.getWidth(), (charRect.y + 16) / texture.getHeight());
                        spriteBatch.setColor(Color.WHITE);
                    }
                }
            }
        }
    }

    private Rectangle CharFinder(char c)
    {
        if (characterList.containsKey((int)c))
        {
            return new Rectangle(characterList.get((int) c).x, characterList.get((int) c).y, 16, 16);
        }
        else
        {
            return new Rectangle(0,0,16,16);
        }
    }
}