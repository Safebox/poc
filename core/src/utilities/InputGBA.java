package utilities;

import com.badlogic.gdx.Input;

public class InputGBA
{
    public static final int ButtonA = Input.Keys.L;
    public static final int ButtonB = Input.Keys.K;
    public static final int ButtonSEL = Input.Keys.SPACE;
    public static final int ButtonESC = Input.Keys.ESCAPE;
    public static final int ButtonRIGHT = Input.Keys.D;
    public static final int ButtonLEFT = Input.Keys.A;
    public static final int ButtonUP = Input.Keys.W;
    public static final int ButtonDOWN = Input.Keys.S;
    public static final int ButtonR = Input.Keys.O;
    public static final int ButtonL = Input.Keys.Q;
}