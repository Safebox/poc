package utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Require's GNU Unifont's Plane 0 cropped to 4096 x 4096 as a base. Available at: http://unifoundry.com/unifont/index.html
 */
public class UnifontLoaderV3
{
    //Variables
    Texture texture;
    List<Vector2> characterList;
    List<Integer> diacriticList;

    //Properties
    public Texture getTexture()
    {
        return texture;
    }

    //Constructors
    public UnifontLoaderV3()
    {
        this(new Texture("Unifont.png"));
    }

    public UnifontLoaderV3(Texture texture)
    {
        this.texture = texture;
        this.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        characterList = new ArrayList<Vector2>();
        diacriticList = new ArrayList<Integer>();
        for (int c = 0; c < (this.texture.getWidth() / 16) * (this.texture.getHeight() / 16); c++)
        {
            characterList.add(new Vector2((int) ((c * 16f) % (this.texture.getWidth())), (int) ((c * 16f) / (this.texture.getWidth())) * 16));
            if (Character.getType((char) c) == Character.NON_SPACING_MARK || Character.getType((char) c) == Character.COMBINING_SPACING_MARK)
            {
                diacriticList.add(c);
            }
        }
    }

    //Methods
    public void drawString(SpriteBatch spriteBatch, ScalingViewport viewport, Rectangle area, int verticalOffset, String text, Color color, String... args)
    {
        drawString(spriteBatch, viewport, area, verticalOffset, text, color, 1f, false, args);
    }

    /**
     * @param scale 1 = 8 x 16
     */
    public void drawString(SpriteBatch spriteBatch, ScalingViewport viewport, Rectangle area, int verticalOffset, String text, Color color, float scale, String... args)
    {
        drawString(spriteBatch, viewport, area, verticalOffset, text, color, scale, false, args);
    }

    public void drawString(SpriteBatch spriteBatch, ScalingViewport viewport, Rectangle area, int verticalOffset, String text, Color color, boolean fullwidth, String... args)
    {
        drawString(spriteBatch, viewport, area, verticalOffset, text, color, 1f, fullwidth, args);
    }

    /**
     * @param scale 1 = (fullwidth ? 16 : 8) x 16
     */
    public void drawString(SpriteBatch spriteBatch, ScalingViewport viewport, Rectangle area, int verticalOffset, String text, Color color, float scale, boolean fullwidth, String... args)
    {
        text = argsReplace(text, args);

        int lineEnd = MathUtils.round(area.width / ((fullwidth ? 16f : 8f) * scale));
        int lineSplit = -1;
        for (int c = 0; c < text.length(); c++)
        {
            if (c < lineEnd)
            {
                switch (text.charAt(c))
                {
                    case ' ':
                        if ((c % lineEnd) != 0)
                        {
                            lineSplit = c;
                        }
                        break;
                    case '\n':
                        lineEnd += c;
                        break;
                    case '{':
                    case '}':
                        lineEnd += 2;
                        break;
                }
            }
            else
            {
                text = text.substring(0, lineSplit) + '\n' + text.substring(lineSplit + 1, text.length());
                lineEnd += MathUtils.round(area.width / ((fullwidth ? 16f : 8f) * scale));
            }
        }

        float viewportScale = Math.min(viewport.getScreenWidth() / viewport.getWorldWidth(), viewport.getScreenHeight() / viewport.getWorldHeight());
        spriteBatch.end();
        Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST);
        Gdx.gl.glScissor(MathUtils.round(area.x + viewport.getScreenX()), MathUtils.round((area.y * viewportScale) + viewport.getScreenY() - (area.height * viewportScale)), MathUtils.round(area.width * viewportScale), MathUtils.round(area.height * viewportScale));
        spriteBatch.begin();
        render(spriteBatch, new Rectangle(area.x, area.y + verticalOffset, area.width, area.height), text, color, scale, fullwidth);
        spriteBatch.end();
        Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST);
        spriteBatch.begin();
    }

    private String argsReplace(String text, String... args)
    {
        Pattern pattern = Pattern.compile("\\{(\\d+)\\}");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find())
        {
            if (Integer.valueOf(matcher.group(1)) < args.length)
            {
                text = text.replace("{" + matcher.group(1) + "}", args[Integer.valueOf(matcher.group(1))]);
            }
        }
        return text;
    }

    private void render(SpriteBatch spriteBatch, Rectangle area, String text, Color color, float scale, boolean fullwidth)
    {
        Pattern pattern = Pattern.compile("(?s)\\{([*\\/_-])|([*\\/_-])\\}|\\{(.*?)\\}");
        Vector2 posOffset = new Vector2();

        Matcher matcher = pattern.matcher(text = text.replaceAll("\t", (fullwidth ? "  " : "    ")));
        Map<Integer, Type> modifierPositions = new HashMap();
        Map<Type, Boolean> modifiers = new HashMap<>();
        while (matcher.find())
        {
            for (int g = 1; g < 3; g++)
            {
                if (matcher.group(g) != null)
                {
                    String k = matcher.group(g);
                    modifierPositions.put(matcher.start(), Type.get(matcher.group(g).charAt(0)));
                    modifiers.put(Type.get(matcher.group(g).charAt(0)), false);
                }
            }
        }
        for (int i = 0; i < text.length(); i++)
        {
            if (modifierPositions.containsKey(i))
            {
                modifiers.put(modifierPositions.get(i), !modifiers.get(modifierPositions.get(i)));
                i += 1;
                if (i >= text.length())
                {
                    break;
                }
                else
                {
                    continue;
                }
            }

            if (text.charAt(i) != '\n')
            {
                float italicOffset = formatItalicBegin(spriteBatch, area, posOffset, fullwidth, modifiers);
                drawCharacter(spriteBatch, new Vector2(area.x + posOffset.x + italicOffset, area.y + posOffset.y), charFinder(text.charAt(i)), color, scale);
                formatBold(spriteBatch, area, posOffset, italicOffset, text.charAt(i), color, scale, modifiers);
                formatUnderline(spriteBatch, area, posOffset, italicOffset, color, scale, fullwidth, modifiers);
                formatStrikethrough(spriteBatch, area, posOffset, italicOffset, color, scale, fullwidth, modifiers);
                formatItalicEnd(spriteBatch, modifiers);
            }
            isFirstLine_isDiacritic_incrementCursor(posOffset, text.charAt(i), scale, fullwidth);
        }
    }

    private void formatBold(SpriteBatch spriteBatch, Rectangle area, Vector2 posOffset, float italicOffset, char token, Color color, float scale, Map<Type, Boolean> modifiers)
    {
        if (modifiers.containsKey(Type.BOLD) && modifiers.get(Type.BOLD))
        {
            drawCharacter(spriteBatch, new Vector2(area.x + posOffset.x + scale + italicOffset, area.y + posOffset.y), charFinder(token), color, scale);
        }
    }

    private void formatUnderline(SpriteBatch spriteBatch, Rectangle area, Vector2 posOffset, float italicOffset, Color color, float scale, boolean fullwidth, Map<Type, Boolean> modifiers)
    {
        if (modifiers.containsKey(Type.UNDERLINE) && modifiers.get(Type.UNDERLINE))
        {
            drawCharacter(spriteBatch, new Vector2(area.x + posOffset.x + italicOffset, area.y + posOffset.y), charFinder('\u0332'), color, scale);
            if (fullwidth)
            {
                drawCharacter(spriteBatch, new Vector2(area.x + posOffset.x + (8f * scale) + italicOffset, area.y + posOffset.y), charFinder('\u0332'), color, scale);
            }
        }
    }

    private void formatStrikethrough(SpriteBatch spriteBatch, Rectangle area, Vector2 posOffset, float italicOffset, Color color, float scale, boolean fullwidth, Map<Type, Boolean> modifiers)
    {
        if (modifiers.containsKey(Type.STRIKETHROUGH) && modifiers.get(Type.STRIKETHROUGH))
        {
            drawCharacter(spriteBatch, new Vector2(area.x + posOffset.x + italicOffset, area.y + posOffset.y), charFinder('\u0336'), color, scale);
            if (fullwidth)
            {
                drawCharacter(spriteBatch, new Vector2(area.x + posOffset.x + (8f * scale) + italicOffset, area.y + posOffset.y), charFinder('\u0336'), color, scale);
            }
        }
    }

    private float formatItalicBegin(SpriteBatch spriteBatch, Rectangle area, Vector2 posOffset, boolean fullwidth, Map<Type, Boolean> modifiers)
    {
        if (modifiers.containsKey(Type.ITALIC) && modifiers.get(Type.ITALIC))
        {
            spriteBatch.end();
            spriteBatch.setTransformMatrix(new Matrix4().setAsAffine(new Affine2().shear(0.25f, 0f)));
            spriteBatch.begin();
            return -(0.25f * (area.y + posOffset.y)) + ((fullwidth ? 16f : 8f) * 0.25f);
        }
        return 0;
    }

    private void formatItalicEnd(SpriteBatch spriteBatch, Map<Type, Boolean> modifiers)
    {
        if (modifiers.containsKey(Type.ITALIC) && modifiers.get(Type.ITALIC))
        {
            spriteBatch.end();
            spriteBatch.setTransformMatrix(new Matrix4());
            spriteBatch.begin();
        }
    }

    private boolean isFirstLine_isDiacritic_incrementCursor(Vector2 posOffset, char token, float scale, boolean fullwidth)
    {
        if (token == '\n')
        {
            posOffset.x = 0;
            posOffset.y -= 16f * scale;
            return true;
        }
        else
        {
            boolean isDiacritic = false;
            for (int d : diacriticList)
            {
                if (token == d)
                {
                    isDiacritic = true;
                    break;
                }
            }
            if (!isDiacritic)
            {
                incrementCursor(posOffset, scale, fullwidth);
            }
            return false;
        }
    }

    private Rectangle charFinder(char c)
    {
        return new Rectangle(characterList.get(c).x, characterList.get(c).y, 16, 16);
    }

    private void incrementCursor(Vector2 posOffset, float scale, boolean fullwidth)
    {
        posOffset.x += 16f * scale * (fullwidth ? 1f : 0.5f);
    }

    private void drawCharacter(SpriteBatch spriteBatch, Vector2 pos, Rectangle charRect, Color color, float scale)
    {
        for (int j = 0; j < 1f / scale; j++)
        {
            spriteBatch.setColor(color);
            spriteBatch.draw(texture, pos.x, pos.y, charRect.getWidth() * scale, -charRect.getHeight() * scale, charRect.x / texture.getWidth(), charRect.y / texture.getHeight(), (charRect.x + 16) / texture.getWidth(), (charRect.y + 16) / texture.getHeight());
            spriteBatch.setColor(Color.WHITE);
        }
    }

    private enum Type
    {
        BOLD('*'),
        ITALIC('/'),
        UNDERLINE('_'),
        STRIKETHROUGH('-');

        private char value;

        Type(char value)
        {
            this.value = value;
        }

        public static Type get(char value)
        {
            for (Type type : values())
            {
                if (type.value == value)
                {
                    return type;
                }
            }
            return null;
        }
    }
}