package utilities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Require's GNU Unifont's Plane 0 cropped to 4096 x 4096 as a base. Available at: http://unifoundry.com/unifont/index.html
 */
public class UnifontLoader
{
    //Variables
    Texture texture;
    Vector2[] characterList;
    int[] diacriticList;

    //Properties
    public Texture Texture()
    {
        return texture;
    }

    //Constructors
    public UnifontLoader()
    {
        texture = new Texture("Unifont.png");
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        characterList = new Vector2[(texture.getWidth() / 16) * (texture.getHeight() / 16)];
        diacriticList = new int[1247];
        int d = 0;
        for (int c = 0; c < characterList.length; c++)
        {
            characterList[c] = new Vector2((int) ((c * 16f) % (texture.getWidth())), (int) ((c * 16f) / (texture.getWidth())) * 16);
            if (Character.getType((char) c) == Character.NON_SPACING_MARK || Character.getType((char) c) == Character.COMBINING_SPACING_MARK)
            {
                diacriticList[d] = c;
                d++;
            }
        }
    }

    //Methods
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color)
    {
        DrawString(spriteBatch, position, text, color, 1f, false);
    }

    /**
     * @param scale 1 = 8 x 16
     */
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, float scale)
    {
        DrawString(spriteBatch, position, text, color, scale, false);
    }

    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, boolean fullwidth)
    {
        DrawString(spriteBatch, position, text, color, 1f, fullwidth);
    }

    /**
     * @param scale 1 = (fullwidth ? 16 : 8) x 16
     */
    public void DrawString(SpriteBatch spriteBatch, Vector2 position, String text, Color color, float scale, boolean fullwidth)
    {
        Vector2 posOffset = new Vector2();
        boolean firstGlyphOfLine = true;

        for (int i = 0; i < text.length(); i++)
        {
            if (text.charAt(i) == '\n')
            {
                posOffset.x = 0;
                posOffset.y -= 16f * scale;
                firstGlyphOfLine = true;
            }
            else if (text.charAt(i) == '\t')
            {
                position.x += (16f * 3f) * scale * (fullwidth ? 1f : 0.5f);
            }
            else
            {
                if (firstGlyphOfLine)
                {
                    posOffset.x = 0;
                    firstGlyphOfLine = false;
                }
                else
                {
                    boolean isDiacritic = false;
                    for (int d : diacriticList)
                    {
                        if (text.charAt(i) == d)
                        {
                            isDiacritic = true;
                            break;
                        }
                    }
                    if (!isDiacritic)
                    {
                        posOffset.x += 16f * scale * (fullwidth ? 1f : 0.5f);
                    }
                }

                if (text.charAt(i) != '\r' && text.charAt(i) != '\n')
                {
                    Rectangle charRect = CharFinder(text.charAt(i));
                    Vector2 pos = new Vector2(position.x + posOffset.x, position.y + posOffset.y);
                    for (int j = 0; j < 1f / scale; j++)
                    {
                        spriteBatch.setColor(color);
                        spriteBatch.draw(texture, pos.x, pos.y, charRect.getWidth() * scale, -charRect.getHeight() * scale, charRect.x / texture.getWidth(), charRect.y / texture.getHeight(), (charRect.x + 16) / texture.getWidth(), (charRect.y + 16) / texture.getHeight());
                        spriteBatch.setColor(Color.WHITE);
                    }
                }
            }
        }
    }

    private Rectangle CharFinder(char c)
    {
        return new Rectangle(characterList[c].x, characterList[c].y, 16, 16);
    }
}